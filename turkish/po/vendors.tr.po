# Turkish messages for Debian Web pages (debian-webwml).
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as Debian Web pages.
#
# Murat Demirten <murat@debian.org>, 2002, 2003, 2004.
# Recai Oktaş <roktas@omu.edu.tr>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-11-10 15:06+0100\n"
"PO-Revision-Date: 2019-01-18 00:45+0100\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Satıcı"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Debian'a katkı kabul ediyor"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Mimariler"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Uluslararası Sipariş"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "İletişim"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Satıcı Ev sayfası"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "sayfa"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "eposta"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "Avrupa içinde"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "Bazı bölgelere"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "kaynak"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "ve"

#~ msgid "Official CD"
#~ msgstr "Resmi CD"

#~ msgid "Development Snapshot"
#~ msgstr "Geliştirme Sürümü"

#~ msgid "Vendor Release"
#~ msgstr "Satıcı Sürümü"

#~ msgid "Multiple Distribution"
#~ msgstr "Çoklu Dağıtım"

#~ msgid "non-US included"
#~ msgstr "non-US dahil"

#~ msgid "non-free included"
#~ msgstr "non-free dahil"

#~ msgid "contrib included"
#~ msgstr "contrib dahil"

#~ msgid "vendor additions"
#~ msgstr "satıcı eklemeleri"

#~ msgid "Custom Release"
#~ msgstr "Özel Sürüm"

#~ msgid "reseller of $var"
#~ msgstr "$var için tekrar dağıtıcı"

#~ msgid "reseller"
#~ msgstr "tekrar dağıtıcı"

#~ msgid "updated weekly"
#~ msgstr "haftalık güncelleniyor"

#~ msgid "updated twice weekly"
#~ msgstr "haftada iki defa güncelleniyor"

#~ msgid "updated monthly"
#~ msgstr "aylık güncelleniyor"

#~ msgid "Architectures:"
#~ msgstr "Mimariler:"

#~ msgid "DVD Type:"
#~ msgstr "DVD tipi:"

#~ msgid "CD Type:"
#~ msgstr "CD tipi:"

#~ msgid "email:"
#~ msgstr "eposta:"

#~ msgid "Ship International:"
#~ msgstr "Uluslararası "

#~ msgid "Country:"
#~ msgstr "Ülke:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Debian'a Katkıya İzin veriyor:"

#~ msgid "URL for Debian Page:"
#~ msgstr "Debian Sayfası için URL:"

#~ msgid "Vendor:"
#~ msgstr "Satıcı:"
