<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in Ceph, a distributed storage
and file system: The cephx authentication protocol was suspectible to
replay attacks and calculated signatures incorrectly, <q>ceph mon</q> did not
validate capabilities for pool operations (resulting in potential
corruption or deletion of snapshot images) and a format string
vulnerability in libradosstriper could result in denial of service.</p>

<p>For the stable distribution (stretch), these problems have been fixed in
version 10.2.11-1.</p>

<p>We recommend that you upgrade your ceph packages.</p>

<p>For the detailed security status of ceph please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ceph">https://security-tracker.debian.org/tracker/ceph</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4339.data"
# $Id: $
