<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was found in the WPA protocol implementation found in
wpa_supplication (station) and hostapd (access point).</p>

<p>The EAP-pwd implementation in hostapd (EAP server) and wpa_supplicant (EAP
peer) doesn't properly validate fragmentation reassembly state when receiving
an unexpected fragment. This could lead to a process crash due to a NULL
pointer derefrence.</p>

<p>An attacker in radio range of a station or access point with EAP-pwd support
could cause a crash of the relevant process (wpa_supplicant or hostapd),
ensuring a denial of service.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 2:2.4-1+deb9u4.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>For the detailed security status of wpa please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4450.data"
# $Id: $
