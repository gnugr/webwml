<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>dnsruby is a feature-complete DNS(SEC) client for Ruby. It ships the DNS
Root Key Signing Key (KSK), used as trust anchor to validate the
authenticity of DNS records. This update includes the latest KSK
(KSK-2017), that will be used by ICANN to sign the Root Zone today, 11
October 2018.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.54-2+deb8u1.</p>

<p>We recommend that you upgrade your dnsruby packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1542.data"
# $Id: $
