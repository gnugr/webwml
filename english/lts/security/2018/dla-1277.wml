<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Chris Navarrete from Fortinet's FortiGuard Labs discovered that Audacity,
a multi-track audio editor, contains a vulnerability such that a .wav
file with a crafted FORMATCHUNK structure (many channels) can result in
a denial of service (memory corruption and application crash).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.1-1+deb7u1.</p>

<p>We recommend that you upgrade your audacity packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1277.data"
# $Id: $
