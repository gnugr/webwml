<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that Mono’s string-to-double parser may crash, on
specially crafted input. This could lead to arbitrary code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1002208">CVE-2018-1002208</a>

<p>Mono embeds the sharplibzip library which is
vulnerable to directory traversal, allowing attackers to write to
arbitrary files via a ../ (dot dot slash) in a Zip archive entry that
is mishandled during extraction. This vulnerability is also known as
'Zip-Slip'.</p>

<p>The Mono developers intend to entirely remove sharplibzip from the
sources and do not plan to fix this issue. It is therefore recommended
to fetch the latest sharplibzip version by using the nuget package
manager instead. The embedded version should not be used with
untrusted zip files.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.2.8+dfsg-10+deb8u1.</p>

<p>We recommend that you upgrade your mono packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1564.data"
# $Id: $
