<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been found in the Apache HTTPD server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15710">CVE-2017-15710</a>

    <p>Alex Nichols and Jakob Hirsch reported that mod_authnz_ldap, if
    configured with AuthLDAPCharsetConfig, could cause an of bound write
    if supplied with a crafted Accept-Language header. This could
    potentially be used for a Denial of Service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1301">CVE-2018-1301</a>

    <p>Robert Swiecki reported that a specially crafted request could have
    crashed the Apache HTTP Server, due to an out of bound access after
    a size limit is reached by reading the HTTP header.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1312">CVE-2018-1312</a>

    <p>Nicolas Daniels discovered that when generating an HTTP Digest
    authentication challenge, the nonce sent by mod_auth_digest to
    prevent reply attacks was not correctly generated using a
    pseudo-random seed. In a cluster of servers using a common Digest
    authentication configuration, HTTP requests could be replayed across
    servers by an attacker without detection.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.2.22-13+deb7u13.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1389.data"
# $Id: $
