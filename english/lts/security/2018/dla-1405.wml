<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Libgcrypt is prone to a local side-channel attack
allowing recovery of ECDSA private keys.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.6.3-2+deb8u5.</p>

<p>We recommend that you upgrade your libgcrypt20 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1405.data"
# $Id: $
