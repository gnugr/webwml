<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>David Sopas discovered that Kohana, a PHP framework, was vulnerable to
a Cross-site scripting (XSS) attack that allowed remote attackers to
inject arbitrary web script or HTML by bypassing the strip_image_tags
protection mechanism in system/classes/Kohana/Security.php. This issue
was resolved by permanently removing the strip_image_tags function.
Users are advised to sanitize user input by using external libraries
instead.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.3.4-2+deb7u1.</p>

<p>We recommend that you upgrade your libkohana2-php packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1241.data"
# $Id: $
