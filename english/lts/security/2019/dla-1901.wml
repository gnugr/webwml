<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Nick Roessler and Rafi Rubin discovered that the IMAP and ManageSieve
protocol parsers in the Dovecot email server do not properly validate
input (both pre- and post-login). A remote attacker can take advantage
of this flaw to trigger out of bounds heap memory writes, leading to
information leaks or potentially the execution of arbitrary code.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:2.2.13-12~deb8u7.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1901.data"
# $Id: $
