<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in Ghostscript, the GPL PostScript/PDF
interpreter, which may result in denial of service or the execution of
arbitrary code if a malformed Postscript file is processed (despite the
-dSAFER sandbox being enabled).</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
9.26a~dfsg-0+deb8u3.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1792.data"
# $Id: $
