<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The PostgreSQL project has release a new minor release of the 9.4
branch.</p>

<p>For Debian 8 <q>Jessie</q>, this has been uploaded as version
9.4.22-0+deb8u1.</p>

<p>We recommend that you upgrade your postgresql-9.4 packages.</p>

<p>Note that the end of life of the 9.4 branch is scheduled for February
2020. Please consider upgrading to a newer major release before that
point.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in postgresql-9.4 version 9.4.22-0+deb8u1</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1784.data"
# $Id: $
