<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4861">CVE-2016-4861</a>

      <p>The implementation of ORDER BY and GROUP BY in Zend_Db_Select
      remained prone to SQL injection when a combination of SQL
      expressions and comments were used. This security patch provides
      a comprehensive solution that identifies and removes comments
      prior to checking validity of the statement to ensure no SQLi
      vectors occur.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.11.13-1.1+deb7u5.</p>

<p>We recommend that you upgrade your zendframework packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-646.data"
# $Id: $
