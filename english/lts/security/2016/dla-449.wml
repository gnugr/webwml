<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were found in botan1.10, a C++
library which provides support for many common cryptographic
operations, including encryption, authentication, X.509v3 certificates
and CRLs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9742">CVE-2014-9742</a>

    <p>A bug in Miller-Rabin primality testing was responsible for
    insufficient randomness.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5726">CVE-2015-5726</a>

    <p>The BER decoder would crash due to reading from offset 0 of an
    empty vector if it encountered a BIT STRING which did not contain
    any data at all. This can be used to easily crash applications
    reading untrusted ASN.1 data, but does not seem exploitable for
    code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5727">CVE-2015-5727</a>

    <p>The BER decoder would allocate a fairly arbitrary amount of memory
    in a length field, even if there was no chance the read request
    would succeed. This might cause the process to run out of memory or
    invoke the OOM killer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7827">CVE-2015-7827</a>

    <p>Use constant time PKCS #1 unpadding to avoid possible side channel
    attack against RSA decryption</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2194">CVE-2016-2194</a>

    <p>Infinite loop in modular square root algorithm.
    The ressol function implementing the Tonelli-Shanks algorithm for
    finding square roots could be sent into a nearly infinite loop due
    to a misplaced conditional check. This could occur if a composite
    modulus is provided, as this algorithm is only defined for primes.
    This function is exposed to attacker controlled input via the
    OS2ECP function during ECC point decompression.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2195">CVE-2016-2195</a>

    <p>Fix Heap overflow on invalid ECC point.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2849">CVE-2016-2849</a>

    <p>Use constant time modular inverse algorithm to avoid possible
    side channel attack against ECDSA</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.10.5-1+deb7u1.</p>

<p>We recommend that you upgrade your botan1.10 packages.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-449.data"
# $Id: $
