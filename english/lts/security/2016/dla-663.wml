<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It has been discovered that Tor treats the contents of some buffer
chunks as if they were a NUL-terminated string.  This issue could
enable a remote attacker to crash a Tor client, hidden service, relay,
or authority.  This update aims to defend against this general
class of security bugs.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version 0.2.4.27-2.</p>

<p>For the stable distribution (jessie), this problem has been fixed in
version 0.2.5.12-3., for unstable (sid) with version 0.2.8.9-1, and for
experimental with 0.2.9.4-alpha-1.</p>

<p>Additionally, for wheezy this updates the set of authority directory servers
to the one from Tor 0.2.8.7, released in August 2016.</p>

<p>We recommend that you upgrade your tor packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-663.data"
# $Id: $
