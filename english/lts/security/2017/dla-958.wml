<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9224">CVE-2017-9224</a>

     <p>An issue was discovered in Oniguruma 6.2.0, as used in Oniguruma-mod in
     Ruby through 2.4.1 and mbstring in PHP through 7.1.5. A stack
     out-of-bounds read occurs in match_at() during regular expression
     searching. A logical error involving order of validation and access in
     match_at() could result in an out-of-bounds read from a stack buffer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9226">CVE-2017-9226</a>

     <p>An issue was discovered in Oniguruma 6.2.0, as used in Oniguruma-mod in
     Ruby through 2.4.1 and mbstring in PHP through 7.1.5. A heap
     out-of-bounds write or read occurs in next_state_val() during regular
     expression compilation. Octal numbers larger than 0xff are not handled
     correctly in fetch_token() and fetch_token_in_cc(). A malformed regular
     expression containing an octal number in the form of '\700' would
     produce an invalid code point value larger than 0xff in
     next_state_val(), resulting in an out-of-bounds write memory
     corruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9227">CVE-2017-9227</a>

     <p>An issue was discovered in Oniguruma 6.2.0, as used in Oniguruma-mod in
     Ruby through 2.4.1 and mbstring in PHP through 7.1.5. A stack
     out-of-bounds read occurs in mbc_enc_len() during regular expression
     searching. Invalid handling of reg-&gt;dmin in forward_search_range()
     could result in an invalid pointer dereference, as an out-of-bounds
     read from a stack buffer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9228">CVE-2017-9228</a>

     <p>An issue was discovered in Oniguruma 6.2.0, as used in Oniguruma-mod in
     Ruby through 2.4.1 and mbstring in PHP through 7.1.5. A heap
     out-of-bounds write occurs in bitset_set_range() during regular
     expression compilation due to an uninitialized variable from an
     incorrect state transition. An incorrect state transition in
     parse_char_class() could create an execution path that leaves a
     critical local variable uninitialized until it's used as an index,
     resulting in an out-of-bounds write memory corruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9229">CVE-2017-9229</a>

     <p>An issue was discovered in Oniguruma 6.2.0, as used in Oniguruma-mod in
     Ruby through 2.4.1 and mbstring in PHP through 7.1.5. A SIGSEGV occurs
     in left_adjust_char_head() during regular expression compilation.
     Invalid handling of reg-&gt;dmax in forward_search_range() could result in
     an invalid pointer dereference, normally as an immediate
     denial-of-service condition.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.9.1-1+deb7u1.</p>

<p>We recommend that you upgrade your libonig packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-958.data"
# $Id: $
