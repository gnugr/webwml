<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2016-5240">CVE-2016-5240</a> 
was improperly applied which resulted in
GraphicsMagick crashing instead of entering an infinite loop with the
given proof of concept.</p>

<p>Furthermore, the original announcement mistakently used the identifier
"DLA 574-1" instead of the correct one, "DLA 547-1".</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u6.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-547-2.data"
# $Id: $
