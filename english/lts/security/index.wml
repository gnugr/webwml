#use wml::debian::template title="LTS Security Information" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Keeping your Debian LTS system secure</toc-add-entry>

<p>In order to receive the latest Debian LTS security advisories, subscribe to
the <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a> mailing list.</p>

<p>For more information about security issues in Debian, please refer to
the <a href="../../security">Debian Security Information</a>.</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Recent Advisories</toc-add-entry>

<p>These web pages include a condensed archive of security advisories posted to
the <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a> list.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}
<p>The latest Debian LTS security advisories are also available in
<a href="dla">RDF format</a>. We also offer a
<a href="dla-long">second file</a> that includes the first paragraph
of the corresponding advisory so you can see in it what the advisory is
about.</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>The older security advisories are also available:
<:= get_past_sec_list(); :>

<p>Debian distributions are not vulnerable to all security problems. The
<a href="https://security-tracker.debian.org/">Debian Security Tracker</a> 
collects all information about the vulnerability status of Debian packages,
and can be searched by CVE name or by package.</p>


<toc-add-entry name="contact">Contact Information</toc-add-entry>

<p>Please read the <a href="https://wiki.debian.org/LTS/FAQ">Debian LTS FAQ</a> before contacting us,
your question may well be answered there already!</p>

<p>The <a href="https://wiki.debian.org/LTS/FAQ">contact information is in the FAQ</a> as
well.</p>
