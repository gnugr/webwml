msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:28+0200\n"
"Last-Translator: Vardan Gevorgyan <vgevorgyan@vtgsoftware.com>\n"
"Language-Team: Armenian <translation-team-hy@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/searchtmpl/search.def:6
msgid "Search for"
msgstr "Փնտրել"

#: ../../english/searchtmpl/search.def:10
msgid "Results per page"
msgstr "Արդյունքները ըստ էջերի"

#: ../../english/searchtmpl/search.def:14
msgid "Output format"
msgstr "Արտածման ֆորմատը"

#: ../../english/searchtmpl/search.def:18
msgid "Long"
msgstr "Երկար"

#: ../../english/searchtmpl/search.def:22
msgid "Short"
msgstr "Կարճ"

#: ../../english/searchtmpl/search.def:26
msgid "URL"
msgstr "URL"

#: ../../english/searchtmpl/search.def:30
msgid "Default query type"
msgstr "Հարցման հիմնական տեսակ"

#: ../../english/searchtmpl/search.def:34
msgid "All Words"
msgstr "Բոլոր բառերը"

#: ../../english/searchtmpl/search.def:38
msgid "Any Words"
msgstr "Ցանկացած բառ"

#: ../../english/searchtmpl/search.def:42
msgid "Search through"
msgstr "Փնտրել տվյալ վայրում"

#: ../../english/searchtmpl/search.def:46
msgid "Entire site"
msgstr "Ամբողջական կայքում"

#: ../../english/searchtmpl/search.def:50
msgid "Language"
msgstr "Լեզու"

#: ../../english/searchtmpl/search.def:54
msgid "Any"
msgstr "Յուրաքանչյուր"

#: ../../english/searchtmpl/search.def:58
msgid "Search results"
msgstr "Փնտրման արդյունքները"

#: ../../english/searchtmpl/search.def:65
msgid ""
"Displaying documents \\$(first)-\\$(last) of total <B>\\$(total)</B> found."
msgstr "<B>\\$(total)</B> փնտրվածներից ցույց է տրվում \\$(first)-\\$(last)-ը"

#: ../../english/searchtmpl/search.def:69
msgid ""
"Sorry, but search returned no results. <P>Some pages may be available in "
"English only, you may want to try searching again and set the Language to "
"Any."
msgstr "Կներեք, բայց փնտրման ընթացքում արդյունք չի հայտնաբերվել։ <P> Որոշ"
"էջեր կան միայն Անգլերեն լեզվով, դուք պետք է կրկին կատարեք փնտրումը Լեզուն գնելով Ցանկացած։"

#: ../../english/searchtmpl/search.def:73
msgid "An error occured!"
msgstr "Սխալ է տեղի ունեցել"

#: ../../english/searchtmpl/search.def:77
msgid "You should give at least one word to search for."
msgstr "Դուք պետք է ունենաք գոնե մեկ բառ փնտրելու համար։"

#: ../../english/searchtmpl/search.def:81
msgid "Powered by"
msgstr ""

#: ../../english/searchtmpl/search.def:85
msgid "Next"
msgstr "Հաջորդ"

#: ../../english/searchtmpl/search.def:89
msgid "Prev"
msgstr "Նախորդ"
