#use wml::debian::template title="Erratas del instalador de Debian"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5863207a091bcfec9124de413b37739d67e43557" maintainer="Laura Arjona Reina"

<h1>Erratas en «<humanversion />»</h1>

<p>
Esta es una lista de problemas conocidos en la versión «<humanversion />»
del instalador de Debian. Si usted no ve aquí listado su problema,
por favor envíenos un <a href="$(HOME)/releases/stable/amd64/ch05s04.html#submit-bug">informe de instalación</a>
describiéndolo.
</p>

<dl class="gloss">

<dt>GNOME puede fallar al iniciarse en algunas instalaciones de máquina virtual</dt>
  <dd>Durante las pruebas de la imagen de Stretch Alfa 4, se ha visto que GNOME
  puede fallar al iniciarse dependiendo de la instalación usada en máquinas virtuales.
  Parece que usar cirrus como una emulación del chip de vídeo funciona bien.
  <br/>
  <b>Estado:</b> Se está investigando.</dd>

<dt>Las instalaciones con escritorio pueden fallar si se utiliza solo el CD 1</dt>
<dd>Debido a restricciones de espacio en el primer CD, no caben en el CD 1 todos los 
paquetes que se esperan para el escritorio GNOME. Para una instalación exitosa, 
use fuentes de paquete adicionales (p. ej. un segundo CD o una réplica en red)
o use un DVD.
<br /> <b>Estado:</b> No es probable que se puedan hacer más esfuerzos para encajar paquetes
	en el CD 1.</dd>

<dt>Tema usado en el instalador</dt>
     <dd>Todavía no hay arte gráfico para Buster, y el instalador aún usa
     el tema de Stretch.
     <br />
     <b>Estado:</b>
       Corregido en Buster RC 1: El tema <a href="https://bits.debian.org/2019/01/futurePrototype-will-be-the-default-theme-for-debian-10.html">
       futurePrototype</a> se ha integrado.</dd>
       
      <dt>Fallos en los métodos abreviados en el editor nano</dt>
      <dd>La versión de nano usada dentro del instalador de Debian no 
      utiliza los mismos métodos abreviados que el paquete nano estándar 
      (en sistemas instalados). En particular la sugerencia de usar <tt>C-s</tt>
      para <q>Guardar</q> parece ser un problema para las personas que usan
      el instalador en una consola en serie
      (<a href="https://bugs.debian.org/915017">#915017</a>).
      <br />
      <b>Estado:</b> Corregido en Buster RC 1.</dd>
 
     <dt>Soporte TLS en wget estropeado</dt>
     <dd>Un cambio en la biblioteca openssl ha conducido a algunos problemas para
     los usuarios, ya que un fichero de configuración que era inicialmente opcional
     pasó repentinamente a ser obligatorio. Como consecuencia, al menos
     <tt>wget</tt> no soporta HTTPS dentro del instalador
       (<a href="https://bugs.debian.org/926315">#926315</a>).
     <br />
     <b>Estado:</b> Corregido en Buster RC 2.</dd>

     <dt>Problemas relacionados con la entropía</dt>
     <dd>Incluso con soporte HTTPS funcional en wget, puede haber problemas
     relacionados con la entropía, dependiendo de la disponibilidad de un
     generador de números aleatorios («RNG») por hardware o de que haya un número
     suficiente de eventos para alimentar las reservas de entropía
       (<a href="https://bugs.debian.org/923675">#923675</a>). Los síntomas 
       habituales pueden ser un retraso largo cuando ocurre la primera conexión
       HTTPS, o durante la generación del par de claves de SSH.
     <br />
     <b>Estado:</b> Corregido en Buster RC 2.</dd>

     <dt>LUKS2 es incompatible con el soporte a cifrado de disco de GRUB</dt>
     <dd>Se descubrió tarde que GRUB no tiene soporte para LUKS2.
     Esto significa que los usuarios que quieren usar
       <tt>GRUB_ENABLE_CRYPTODISK</tt> y evitar una partición
       <tt>/boot</tt> separada y sin cifrar no podrán hacerlo
       (<a href="https://bugs.debian.org/927165">#927165</a>).  En cualquier caso, 
       esta configuración no está soportada en el instalador, pero podría tener
       sentido al menos documentar esta limitación de manera más prominente
       y tener al menos la posibilidad de optar por LUKS1 durante el proceso de
       instalación.
     <br />
     <b>Estado:</b> Se han expresado algunas ideas en el informe de fallo; los mantenedores de cryptsetup
     han escrito <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">documentación específica</a>.</dd>

     <dt>Fallo al preconfigurar repositorios APT personalizados</dt>
     <dd>Es habitual especificar también claves extra al configurar 
     repositorios APT personalizados, usando parámetros como
     <tt>apt-setup/local0/key</tt>. Debido a cambios en el modo de funcionamiento
       de <tt>apt-key</tt>, intentar especificar esta configuración 
       resultaría en fallos con el instalador de Buster
       (<a href="https://bugs.debian.org/851774">851774</a>).
     <br />
     <b>Estado:</b>
       El <a href="https://bugs.debian.org/851774#68">parche
       propuesto</a> no ha sido revisado a tiempo para la publicación inicial de
       Buster, pero se espera que se pruebe durante el ciclo de desarrollo de
       Bullseye y que al final se incluya en los «backports» en alguna publicación
       de versión menor de Buster.</dd>
          
 	 <!-- las cosas deberían ir mejor a partir de Jessie Beta 2...

         <dt>Soporte GNU/kFreeBSD</dt>
         <dd>Las imágenes de instalación de GNU/kFreeBSD tienen varios 
         fallos importantes
         (<a href="https://bugs.debian.org/757985">#757985</a>,
         <a href="https://bugs.debian.org/757986">#757986</a>,
         <a href="https://bugs.debian.org/757987">#757987</a>,
        <a href="https://bugs.debian.org/757988">#757988</a>). Los adaptadores
        agradecerían manos que ayuden a volver a tener en forma el instalador!</dd>
-->
<!-- algo obsoleto por el primer "cambio importante" mencionado en el anuncio de 20140813...
	<dt>Accesibilidad del sistema instalado</dt>
	<dd>Aunque se usen tecnologías de accesibilidad durante el proceso de 
	instalación, puede que éstas no se habiliten de manera automática en el sistema
	instalado.
	</dd>
-->

<!-- se deja esto para un posible futuro uso...
	<dt>Las instalaciones de escritorio en i386 no funcionan usando sólo el CD#1</dt>
	<dd>Debido a las restricciones de espacio en el primer CD, no todos los paquetes
	esperados para el escritorio GNOME caben en el CD#1. Para una instalación exitosa,
	use fuentes de paquetes extra (p.ej. un segundo CD o una réplica en la red) o use
	un DVD.
	<br />
	<b>Estado:</b> No es probable que se puedan hacer más esfuerzos para encajar paquetes
	en el CD#1.
	</dd>
-->

<!-- ditto...
	<dt>Posibles problemas con el arranque UEFI en amd64</dt>
	<dd>Ha habido algunos informes de problemas para arrancar el instalador de
	Debian en modo UEFI en sistemas amd64. Algunos sistemas aparentemente no arrancan
	de manera fiable usando <code>grub-efi</code>, y algunos otros muestran problemas
	de corrupción de los gráficos al visualizar la pantalla inicial («splash») de la instalación.
	<br />
	Si encuentra alguno de estos problemas, por favor remita un informe de error dando el máximo
	detalle posible, tanto sobre los síntomas, como sobre su hardware - esto debería ayudar al 
	equipo a resolver estos fallos. Para rodear el problema, por ahora, pruebe a desactivar UEFI
	e instalar usando la <q>BIOS antigua («Legacy»)</q> o el <q>modo alternativo («Fallback»)</q>.
	<br />
	<b>Estado:</b> Pueden aparecer más correcciones de fallos en las distintas versiones de Wheezy.
	</dd>
-->

<!-- ditto...
	<dt>i386: se necesita más de 32mb de memoria para instalar</dt>
	<dd>
	La cantidad mínima de memoria que se necesita para instalar en i386
	es 48m, en lugar de los 32mb anteriores. Esperamos reducir los requisitos
	de nuevo a 32mb más adelante. Los requisitos de memoria pueden haber cambiado
	también para otras arquitecturas.
	</dd>
-->

</dl>
