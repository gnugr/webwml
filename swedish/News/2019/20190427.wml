#use wml::debian::translation-check translation="f797e8b952b201d953c4e7de221fd635808136e4" maintainer="Andreas Rönnquist"
<define-tag pagetitle>Uppdaterad Debian 9; 9.9 utgiven</define-tag>
<define-tag release_date>2019-04-27</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin nionde uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>Dom som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på dom vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<p>Som ett specialfall för denna punktutgåva behöver dom som använder
verktyget <q>apt-get</q> för att utföra uppgraderingen säkerställa att dom använder
kommandot <q>dist-upgrade</q>, för att uppdatera till de senaste kärnpaketen.
Användare av andra verktyg så som <q>apt</q> och <q>aptitude</q> bör använda
kommandot <q>upgrade</q>.</p>

<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction audiofile "Fix denial of service [CVE-2018-13440] and buffer overflow issues [CVE-2018-17095]">
<correction base-files "Update for the point release">
<correction bwa "Fix buffer overflow [CVE-2019-10269]">
<correction ca-certificates-java "Fix bashisms in postinst and jks-keystore">
<correction cernlib "Apply optimization flag -O to Fortran modules instead of -O2 which generates broken code; fix build failure on arm64 by disabling PIE for Fortran executables">
<correction choose-mirror "Update included mirror list">
<correction chrony "Fix logging of measurements and statistics, and stopping of chronyd, on some platforms when seccomp filtering is enabled">
<correction ckermit "Drop OpenSSL version check">
<correction clamav "Fix out-of-bounds heap access when scanning PDF documents [CVE-2019-1787], PE files packed using Aspack [CVE-2019-1789] or OLE2 files [CVE-2019-1788]">
<correction dansguardian "Add <q>missingok</q> to logrotate configuration">
<correction debian-installer "Rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-security-support "Update support statuses">
<correction diffoscope "Fix tests to work with Ghostscript 9.26">
<correction dns-root-data "Update root data to 2019031302">
<correction dnsruby "Add new root key (KSK-2017); ruby 2.3.0 deprecates TimeoutError, use Timeout::Error">
<correction dpdk "New upstream stable release">
<correction edk2 "Fix buffer overflow in BlockIo service [CVE-2018-12180]; DNS: Check received packet size before using [CVE-2018-12178]; fix stack overflow with corrupted BMP [CVE-2018-12181]">
<correction firmware-nonfree "atheros / iwlwifi: update BlueTooth firmware [CVE-2018-5383]">
<correction flatpak "Reject all ioctls that the kernel will interpret as TIOCSTI [CVE-2019-10063]">
<correction geant321 "Rebuild against cernlib with fixed Fortran optmisations">
<correction gnome-chemistry-utils "Stop building the obsolete gcu-plugin package">
<correction gocode "gocode-auto-complete-el: Promote auto-complete-el to Pre-Depends to ensure successful upgrades">
<correction gpac "Fix buffer overflows [CVE-2018-7752 CVE-2018-20762], heap overflows [CVE-2018-13005 CVE-2018-13006 CVE-2018-20761], out-of-bounds writes [CVE-2018-20760 CVE-2018-20763]">
<correction icedtea-web "Stop building the browser plugin, no longer works with Firefox 60">
<correction igraph "Fix a crash when loading malformed GraphML files [CVE-2018-20349]">
<correction jabref "Fix XML External Entity attack [CVE-2018-1000652]">
<correction java-common "Remove the default-java-plugin package, as the icedtea-web Xul plugin is being removed">
<correction jquery "Prevent Object.prototype pollution [CVE-2019-11358]">
<correction kauth "Fix insecure handling of arguments in helpers [CVE-2019-7443]">
<correction libdate-holidays-de-perl "Add March 8th (from 2019 onwards) and May 8th (2020 only) as public holidays (Berlin only)">
<correction libdatetime-timezone-perl "Update included data">
<correction libreoffice "Introduce next Japanese gengou era 'Reiwa'; make -core conflict against openjdk-8-jre-headless (= 8u181-b13-2~deb9u1), which had a broken ClassPathURLCheck">
<correction linux "New upstream stable version">
<correction linux-latest "Update for -9 kernel ABI">
<correction mariadb-10.1 "New upstream stable version">
<correction mclibs "Rebuild against cernlib with fixed Fortran optmisations">
<correction ncmpc "Fix NULL pointer dereference [CVE-2018-9240]">
<correction node-superagent "Fix ZIP bomb attacks [CVE-2017-16129]; fix syntax error">
<correction nvidia-graphics-drivers "New upstream stable release [CVE-2018-6260]">
<correction nvidia-settings "New upstream stable release">
<correction obs-build "Do not allow writing to files in the host system [CVE-2017-14804]">
<correction paw "Rebuild against cernlib with fixed Fortran optmisations">
<correction perlbrew "Allow HTTPS CPAN URLs">
<correction postfix "New upstream stable release">
<correction postgresql-9.6 "New upstream stable release">
<correction psk31lx "Make version sort correctly to avoid potential upgrade issues">
<correction publicsuffix "Update included data">
<correction pyca "Add <q>missingok</q> to logrotate configuration">
<correction python-certbot "Revert to debhelper compat 9, to ensure systemd timers are correctly started">
<correction python-cryptography "Remove BIO_callback_ctrl: The prototype differs with the OpenSSL's definition of it after it was changed (fixed) within OpenSSL">
<correction python-django-casclient "Apply django 1.10 middleware fix; python(3)-django-casclient: fix missing dependencies on python(3)-django">
<correction python-mode "Remove support for xemacs21">
<correction python-pip "Properly catch requests' HTTPError in index.py">
<correction python-pykmip "Fix potential denial of service issue [CVE-2018-1000872]">
<correction r-cran-igraph "Fix denial of service via crafted object [CVE-2018-20349]">
<correction rails "Fix information disclosure issues [CVE-2018-16476 CVE-2019-5418], denial of service issue [CVE-2019-5419]">
<correction rsync "Several security fixes for zlib [CVE-2016-9840 CVE-2016-9841 CVE-2016-9842 CVE-2016-9843]">
<correction ruby-i18n "Prevent a remote denial-of-service vulnerability [CVE-2014-10077]">
<correction ruby2.3 "Fix FTBFS">
<correction runc "Fix root privilege escalation vulnerability [CVE-2019-5736]">
<correction systemd "journald: fix assertion failure on journal_file_link_data; tmpfiles: fix <q>e</q> to support shell style globs; mount-util: accept that name_to_handle_at() might fail with EPERM; automount: ack automount requests even when already mounted [CVE-2018-1049]; fix potential root privilege escalation [CVE-2018-15686]">
<correction twitter-bootstrap3 "Fix cross site scripting issue in tooltips or popovers [CVE-2019-8331]">
<correction tzdata "New upstream release">
<correction unzip "Fix buffer overflow in password protected ZIP archives [CVE-2018-1000035]">
<correction vcftools "Fix information disclosure [CVE-2018-11099] and denial of service [CVE-2018-11129 CVE-2018-11130] via crafted files">
<correction vips "Fix NULL function pointer dereference [CVE-2018-7998], uninitialised memory access [CVE-2019-6976]">
<correction waagent "New upstream release, with many Azure fixes [CVE-2019-0804]">
<correction yorick-av "Rescale frame timestamps; set VBV buffer size for MPEG1/2 files">
<correction zziplib "Fix invalid memory access [CVE-2018-6381], bus error [CVE-2018-6540], out-of-bounds read [CVE-2018-7725], crash via crafted zip file [CVE-2018-7726], memory leak [CVE-2018-16548]; reject ZIP file if the size of the central directory and/or the offset of start of central directory point beyond the end of the ZIP file [CVE-2018-6484, CVE-2018-6541, CVE-2018-6869]">
</table>

<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2018 4259 ruby2.3>
<dsa 2018 4332 ruby2.3>
<dsa 2018 4341 mariadb-10.1>
<dsa 2019 4373 coturn>
<dsa 2019 4374 qtbase-opensource-src>
<dsa 2019 4377 rssh>
<dsa 2019 4385 dovecot>
<dsa 2019 4387 openssh>
<dsa 2019 4388 mosquitto>
<dsa 2019 4389 libu2f-host>
<dsa 2019 4390 flatpak>
<dsa 2019 4391 firefox-esr>
<dsa 2019 4392 thunderbird>
<dsa 2019 4393 systemd>
<dsa 2019 4394 rdesktop>
<dsa 2019 4396 ansible>
<dsa 2019 4397 ldb>
<dsa 2019 4398 php7.0>
<dsa 2019 4399 ikiwiki>
<dsa 2019 4400 openssl1.0>
<dsa 2019 4401 wordpress>
<dsa 2019 4402 mumble>
<dsa 2019 4403 php7.0>
<dsa 2019 4405 openjpeg2>
<dsa 2019 4406 waagent>
<dsa 2019 4407 xmltooling>
<dsa 2019 4408 liblivemedia>
<dsa 2019 4409 neutron>
<dsa 2019 4410 openjdk-8>
<dsa 2019 4411 firefox-esr>
<dsa 2019 4412 drupal7>
<dsa 2019 4413 ntfs-3g>
<dsa 2019 4414 libapache2-mod-auth-mellon>
<dsa 2019 4415 passenger>
<dsa 2019 4416 wireshark>
<dsa 2019 4417 firefox-esr>
<dsa 2019 4418 dovecot>
<dsa 2019 4419 twig>
<dsa 2019 4420 thunderbird>
<dsa 2019 4422 apache2>
<dsa 2019 4423 putty>
<dsa 2019 4424 pdns>
<dsa 2019 4425 wget>
<dsa 2019 4426 tryton-server>
<dsa 2019 4427 samba>
<dsa 2019 4428 systemd>
<dsa 2019 4429 spip>
<dsa 2019 4430 wpa>
<dsa 2019 4431 libssh2>
<dsa 2019 4432 ghostscript>
<dsa 2019 4433 ruby2.3>
<dsa 2019 4434 drupal7>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction gcontactsync "Incompatible with newer firefox-esr versions">
<correction google-tasks-sync "Incompatible with newer firefox-esr versions">
<correction mozilla-gnome-kerying "Incompatible with newer firefox-esr versions">
<correction tbdialout "Incompatible with newer thunderbird versions">
<correction timeline "Incompatible with newer thunderbird versions">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>

