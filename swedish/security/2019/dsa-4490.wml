#use wml::debian::translation-check translation="bbdc5fdd9cf31c8e006e51f38f90961f3cb78f42" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i versionshanteringssystemet Subversion.
Projektet Common Vulnerabilities and Exposures identifierar följande
problem:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11782">CVE-2018-11782</a>

	<p>Ace Olszowka rapporterade att Subversions serverprocess svnserve
	kan avslutas när en välformatterad read-only förfrågan producerar ett
	speciellt svar, som leder till överbelastning.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0203">CVE-2019-0203</a>

	<p>Tomas Bortoli rapporterade att Subversions serverprocess svnserve
	kan avslutas när en klient skickar vissa sekvenser av protokollkommandon.
	Om servern är konfigurerad med anonym åtkomst aktiverad kna detta leda
	till en icke auktoriserad fjärröverbelastning.</p></li>

</ul>

<p>För den gamla stabila utgåvan (Stretch) har dessa problem rättats
i version 1.9.5-1+deb9u4.</p>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 1.10.4-1+deb10u1.</p>

<p>Vi rekommenderar att ni uppgraderar era subversion-paket.</p>

<p>För detaljerad säkerhetsstatus om subversion vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/subversion">https://security-tracker.debian.org/tracker/subversion</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4490.data"
