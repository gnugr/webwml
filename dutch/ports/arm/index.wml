#use wml::debian::template title="Debian voor ARM"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc
#use wml::debian::translation-check translation="54fc3592c640a2cd16d06e9d81560ba24f609eff"

<toc-display/>

<toc-add-entry name="about">Debian op arm</toc-add-entry> <p>Op deze
pagina's vindt u informatie over de voortdurende inspanningen om
Debian GNU/Linux geschikt te maken voor verschillende versies van de <a
href="http://en.wikipedia.org/wiki/ARM_architecture">ARM-architectuur</a>,
welke in allerlei soorten systemen aangetroffen wordt, van ingebedde systemen
tot grote serversystemen.</p>

<toc-add-entry name="status">Huidige toestand</toc-add-entry>
<p>Debian biedt volledige ondersteuning voor drie verschillende varianten van
little-endian ARM hardware:</p>

<ul>

<li>De variant (port) <a href="https://wiki.debian.org/ArmEabiPort">ARM EABI</a>
(armel) is bedoeld voor een reeks oudere 32-bits ARM-apparaten, vooral
die welke in NAS-hardware gebruikt wordt en een reeks *plug-computers.</li>

<li>De recentere variant <a href="https://wiki.debian.org/ArmHardFloatPort">ARM
hard-float</a> (armhf) biedt ondersteuning voor recentere, krachtigere 32-bits
apparaten die gebruik maken van versie 7 van de ARM architectuurspecificatie.</li>

<li>De <a href="https://wiki.debian.org/Arm64Port">64-bits ARM</a>
(arm64) variant biedt ondersteuning voor de recentste 64-bits-apparaten die met ARM uitgerust werden.</li>

</ul>

<p>Er bestaan / bestonden andere varianten (ports) voor ARM-hardware in en rond
Debian - zie <a href="https://wiki.debian.org/ArmPorts">de wiki</a>
voor extra links en een overzicht.</p>

<p>Voor een volledig en actueel overzicht van welke hardware wordt ondersteund
door elk van de varianten, kunt u de betreffende wiki-pagina's raadplegen.
Elke week komt er nieuwe ARM-apparatuur uit en het is makkelijker om de
informatie daar actueel te houden.</p>

<toc-add-entry name="availablehw">Hardware die ter beschikking staat van ontwikkelaars van Debian</toc-add-entry>
<p>Verschillende machines staan ter beschikking van ontwikkelaars van
Debian voor hun werk aan het geschikt maken van Debian voor ARM:
abel.debian.org (armel/armhf), asachi.debian.org
(armhf/arm64) en harris.debian.org (armhf). De machines hebben
chroot-omgevingen voor de ontwikkelingswerkzaamheden waar u met
<em>schroot</em> toegang toe heeft. Raadpleeg de <a
href="https://db.debian.org/machines.cgi">machinedatabase</a> voor
bijkomende informatie over deze machines.</p>

<toc-add-entry name="contacts">Contact</toc-add-entry>
<h3>Mailinglijsten</h3>

<p>De mailinglijst over de ARM port van Debian is te bereiken via
<email "debian-arm@lists.debian.org">.
Indien u daarop wenst in te tekenen, moet u een bericht met als onderwerp
<q>subscribe</q> sturen naar <email "debian-arm-request@lists.debian.org">.
De mailinglijst wordt gearchiveerd in de
<a href="https://lists.debian.org/debian-arm/">debian-arm lijstarchieven</a>.</p>

<p>
Het is ook goed om in te tekenen op de mailinglijst
<a href="http://www.arm.linux.org.uk/mailinglists/">linux-arm</a>.</p>

<h3>IRC</h3>

<p>U vindt ons op IRC op <em>irc.debian.org</em> op het kanaal
<em>#debian-arm</em>.</p>

<toc-add-entry name="people">Mensen</toc-add-entry>
<p>
Hier volgt een lijst van betekenisvolle mensen die momenteel betrokken zijn
bij het geschikt maken van Debian voor ARM.
</p>

<ul>

<li>Ian Campbell <email "ijc@debian.org">
<br />
Debian-installatiesysteem, kernel
</li>

<li>Aurelien Jarno <email "aurel32@debian.org">
<br />
Onderhouder van de ARM buildd en algemene ontwikkelaar van de port
</li>

<li>Steve McIntyre <email "steve@einval.com">
<br />
Lokale beheerder van ARM-machines, documentatie en algemene ontwikkelaar van de port
</li>

<li>Martin Michlmayr <email "tbm@cyrius.com">
<br />
Documentatie, debian-installatiesysteem
</li>

<li>Riku Voipio <email "riku.voipio@iki.fi">
<br />
Ontwikkelaar van de armel port, onderhouder van de buildd
</li>

<li>Wookey <email "wookey@wookware.org">
<br />
Documentatie
</li>

</ul>

<toc-add-entry name="dedication">Opgedragen aan</toc-add-entry>

<p>Chris Rutter,
die project-coördinator en autobuilder-coördinator was voor de ARM
port van Debian, en om het leven kwam in een auto-ongeval. We hebben de
release van de ARM port in de Debian GNU/Linux distributie <q>woody</q>
te zijner gedachtenis aan hem opgedragen.</p>

<toc-add-entry name="thanks">Dankwoord</toc-add-entry>

<p>
De volgende mensen hielpen bij het levensvatbaar maken voor Debian van de
ARM port:

Jim Studt, Jim Pick, Scott Bambrough, Peter Naulls, Tor Slettnes,
Phil Blundell, Vincent Sanders
</p>
