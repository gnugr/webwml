msgid ""
msgstr ""
"Project-Id-Version: fa\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Behrad Eslamifar <behrad_es@yahoo.com>\n"
"Language-Team: debian-l10n-persian <debian-l10n-persian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Persian\n"
"X-Poedit-Country: IRAN, ISLAMIC REPUBLIC OF\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../../english/searchtmpl/search.def:6
msgid "Search for"
msgstr "جستجو برای"

#: ../../english/searchtmpl/search.def:10
msgid "Results per page"
msgstr "نتایج در هر صفحه"

#: ../../english/searchtmpl/search.def:14
msgid "Output format"
msgstr "قالب خروجی"

#: ../../english/searchtmpl/search.def:18
msgid "Long"
msgstr "طولانی"

#: ../../english/searchtmpl/search.def:22
msgid "Short"
msgstr "کوتاه"

#: ../../english/searchtmpl/search.def:26
msgid "URL"
msgstr "URL"

#: ../../english/searchtmpl/search.def:30
msgid "Default query type"
msgstr "نوع پیشفرض درخواست"

#: ../../english/searchtmpl/search.def:34
msgid "All Words"
msgstr "تمام لغات"

#: ../../english/searchtmpl/search.def:38
msgid "Any Words"
msgstr "هر کلمه ای"

#: ../../english/searchtmpl/search.def:42
msgid "Search through"
msgstr "جستجو در"

#: ../../english/searchtmpl/search.def:46
msgid "Entire site"
msgstr "تمام سایت"

#: ../../english/searchtmpl/search.def:50
msgid "Language"
msgstr "زبان"

#: ../../english/searchtmpl/search.def:54
msgid "Any"
msgstr "همه"

#: ../../english/searchtmpl/search.def:58
msgid "Search results"
msgstr "نتایج جستجو"

#: ../../english/searchtmpl/search.def:65
msgid "Displaying documents \\$(first)-\\$(last) of total <B>\\$(total)</B> found."
msgstr "نمایش متون از \\$(first)-\\$(last) از مجموع <B>\\$(total)</B> صفحه پیدا شده."

#: ../../english/searchtmpl/search.def:69
msgid "Sorry, but search returned no results. <P>Some pages may be available in English only, you may want to try searching again and set the Language to Any."
msgstr "متاسفانه نتیجه ای برای جستجوی شما یافت نشد، <P> ممکن است صفحه در زبان انگلیسی موجود باشد. شما می توانید مجددا جستجو نمایید و زبان را روی همه قرار دهید."

#: ../../english/searchtmpl/search.def:73
msgid "An error occured!"
msgstr "خطایی اتفاق افتاد!"

#: ../../english/searchtmpl/search.def:77
msgid "You should give at least one word to search for."
msgstr "شما باید حداقل یک کلمه برای جستجو وارد کنید."

#: ../../english/searchtmpl/search.def:81
msgid "Powered by"
msgstr "قدرت یافته با"

#: ../../english/searchtmpl/search.def:85
msgid "Next"
msgstr "بعدی"

#: ../../english/searchtmpl/search.def:89
msgid "Prev"
msgstr "قبلی"

