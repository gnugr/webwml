#use wml::debian::translation-check translation="f797e8b952b201d953c4e7de221fd635808136e4"
<define-tag pagetitle>Opdateret Debian 9: 9.9 udgivet</define-tag>
<define-tag release_date>2019-04-27</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den niende opdatering af 
dets stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer i den 
stabile udgave, sammen med nogle få rettelser af alvorlige problemer.  
Sikkerhedsbulletiner er allerede udgivet separat og der vil blive refereret til 
dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Online-opdatering til denne revision gøres normalt ved at lade 
pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<p>Særligt for denne punktopdatering, dem der anvender værktøjet <q>apt-get</q> 
til at udføre opgraderingen, skal sikre sig at kommandoen <q>dist-upgrade</q> 
anvendes, for at opdatere til de seneste kernepakker.  Brugerre af andre 
værktøjer, så som <q>apt</q> og <q>aptitude</q>, bør anvende kommandoen 
<q>upgrade</q>.</p>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction audiofile 					"Retter lammelsesangreb [CVE-2018-13440] og problemer med bufferoverløb [CVE-2018-17095]">
<correction base-files 					"Opdaterer til punktopdateringen">
<correction bwa 					"Retter bufferoverløb [CVE-2019-10269]">
<correction ca-certificates-java 			"Retter bashismer i postinst og jks-keystore">
<correction cernlib 					"Anvender optimeringsflag -O på Fortran-moduler i stedet for -O2, som genererer defekt kode; retter opbygningsfejl på arm64 ved at deaktivere PIE for udførbare Fortran-filer">
<correction choose-mirror 				"Opdaterer medfølgende filspejlsliste">
<correction chrony 					"Retter logning af målinger og statistikker, samt stopper chronyd på nogle platforme, når seccomp-filtrering af aktiveret">
<correction ckermit 					"Dropper OpenSSL-versionstjek">
<correction clamav 					"Retter heapadgang udenfor grænserne når der scannes PDF-dokumenter [CVE-2019-1787], PE-filer pakket med Aspack [CVE-2019-1789] eller OLE2-filer [CVE-2019-1788]">
<correction dansguardian 				"Tilføjer <q>missingok</q> til opsætning af logrotate">
<correction debian-installer 				"Genopbygger mod proposed-updates">
<correction debian-installer-netboot-images 		"Genopbygger mod proposed-updates">
<correction debian-security-support 			"Opdaterer supportstatus">
<correction diffoscope 					"Retter tests til at fungere med Ghostscript 9.26">
<correction dns-root-data 				"Opdaterer rootdata til 2019031302">
<correction dnsruby 					"Tilføjer ny rootnøgle (KSK-2017); ruby 2.3.0 gør TimeoutError forældet, anvend Timeout::Error">
<correction dpdk 					"Ny stabil opstrømsudgave">
<correction edk2 					"Retter bufferoverløb i BlockIo-service [CVE-2018-12180]; DNS: Kontrollerer størrelse på modtaget pakke før den anvendes [CVE-2018-12178]; retter stakoverløb med korrupt BMP [CVE-2018-12181]">
<correction firmware-nonfree 				"atheros / iwlwifi: Opdaterer BlueTooth-firmware [CVE-2018-5383]">
<correction flatpak 					"Afviser alle ioctl'er, som kerne vil fortolke som TIOCSTI [CVE-2019-10063]">
<correction geant321 					"Genopbygger mod cernlib med faste Fortran-optimeringer">
<correction gnome-chemistry-utils 			"Stopper opbygning af den forældede pakke gcu-plugin">
<correction gocode 					"gocode-auto-complete-el: Forfremmer auto-complete-el til Pre-Depends, for at sikre succesrige opgraderinger">
<correction gpac 					"Retter bufferoverløb [CVE-2018-7752 CVE-2018-20762], heapoverløb [CVE-2018-13005 CVE-2018-13006 CVE-2018-20761], skrivninger udenfor grænserne [CVE-2018-20760 CVE-2018-20763]">
<correction icedtea-web 				"Stopper opbygning af browserplugin, den fungerer ikke længerre med Firefox 60">
<correction igraph 					"Retter et nedbrud når der indlæses misdannede GraphML-filer [CVE-2018-20349]">
<correction jabref 					"Retter XML External Entity-angreb [CVE-2018-1000652]">
<correction java-common 				"Fjerner pakken default-java-plugin, da XUL-plugin'en icedtea-web bliver fjernet">
<correction jquery 					"Forhindrer Object.prototype-forurening [CVE-2019-11358]">
<correction kauth 					"Retter usikker håndtering af parametre i helpers [CVE-2019-7443]">
<correction libdate-holidays-de-perl 			"Tilføjer 8. marts (fra 2019 og frem) og 8. maj (kun 2020) som offentlige fridage (kun i Berlin)">
<correction libdatetime-timezone-perl 			"Opdaterer medfølgende data">
<correction libreoffice 				"Introducerer næste japanske gengou-æra 'Reiwa'; får -core til at være i konflikt med openjdk-8-jre-headless (= 8u181-b13-2~deb9u1), der havde en defekt ClassPathURLCheck">
<correction linux 					"Ny stabil opstrømsversion">
<correction linux-latest 				"Opdaterer til -9 kerne-ABI">
<correction mariadb-10.1 				"Ny stabil opstrømsversion">
<correction mclibs 					"Genopbygger mod cernlib med faste Fortran-optimeringer">
<correction ncmpc 					"Retter NULL-pointerdereference [CVE-2018-9240]">
<correction node-superagent 				"Retter ZIP-bombeangreb [CVE-2017-16129]; retter syntaksfejl">
<correction nvidia-graphics-drivers 			"Ny stabil opstrømsudgave [CVE-2018-6260]">
<correction nvidia-settings 				"Ny stabil opstrømsudgave">
<correction obs-build 					"Tillad ikke skrivning til filer på værtssystemet [CVE-2017-14804]">
<correction paw 					"Genopbygger mod cernlib med faste Fortran-optimeringer">
<correction perlbrew 					"Tillader HTTPS CPAN-URL'er">
<correction postfix 					"Ny stabil opstrømsudgave">
<correction postgresql-9.6 				"Ny stabil opstrømsudgave">
<correction psk31lx 					"Gør versionssortering korrekt for at undgå potentielle opgraderingsproblemer">
<correction publicsuffix 				"Opdaterer medfølgende data">
<correction pyca 					"Tilføjer <q>missingok</q> til opsætning af logrotate">
<correction python-certbot 				"Ruller tilbage til debhelper compat 9, for at sikre at systemd-timere er startet korrekt">
<correction python-cryptography 			"Fjerner BIO_callback_ctrl: Prototype er forskellig fra OpenSSL's definition af den, efter den blev ændret (rettet) i OpenSSL">
<correction python-django-casclient 			"Anvender django 1.10-middlewarerettelse; python(3)-django-casclient: retter mangelende afhængigheder af python(3)-django">
<correction python-mode 				"Fjerner understøttelse af xemacs21">
<correction python-pip 					"Fanger på korrekt vis forespørgslers HTTPError i index.py">
<correction python-pykmip 				"Retter potentielt lammelsesangrebsproblem [CVE-2018-1000872]">
<correction r-cran-igraph 				"Retter lammelsesangreb gennem fabrikeret objekt [CVE-2018-20349]">
<correction rails 					"Retter informationsafsløringsproblemer [CVE-2018-16476 CVE-2019-5418], lammelsesangrebsproblem [CVE-2019-5419]">
<correction rsync 					"Flere sikkerhedsrettelser i zlib [CVE-2016-9840 CVE-2016-9841 CVE-2016-9842 CVE-2016-9843]">
<correction ruby-i18n 					"Forhindrer fjernudført lammelsesangrebssårbarhed [CVE-2014-10077]">
<correction ruby2.3 					"Retter FTBFS">
<correction runc 					"Retter root-rettighedsforøgelsessårbarhed [CVE-2019-5736]">
<correction systemd 					"journald: Retter assertion-fejl på journal_file_link_data; tmpfiles: Retter <q>e</q> til at understøtte globs i shellstil; mount-util: Accepterer at name_to_handle_at() kan fejle med EPERM; automount: accepterer automount-forespørgsler selv når de allerede er mountet [CVE-2018-1049]; retter potentiel root-rettighedsforøgelse [CVE-2018-15686]">
<correction twitter-bootstrap3 				"Retter problem med udførelse af skripter på tværs af websteder i tooltips eller popovers [CVE-2019-8331]">
<correction tzdata 					"Ny opstrømsudgave">
<correction unzip 					"Retter bufferoverløb i adgangskodebeskyttet ZIP-filer [CVE-2018-1000035]">
<correction vcftools 					"Retter informationsafsløring [CVE-2018-11099] og lammelsesangreb [CVE-2018-11129 CVE-2018-11130] gennem fabrikerede filer">
<correction vips 					"Retter NULL-funktionspointerdereference [CVE-2018-7998], uinitialiseret hukommelsestilgang [CVE-2019-6976]">
<correction waagent 					"Ny opstrømsudgave, med mange Azure-rettelser [CVE-2019-0804]">
<correction yorick-av 					"Genskalerer frametidsstemplinger; sætter VBV-bufferstørrelse for MPEG1/2-filer">
<correction zziplib 					"Retter ugyldig hukommelsestilgang [CVE-2018-6381], busfejl [CVE-2018-6540], læsning udenfor grænserne [CVE-2018-7725], nedbrud gennem fabrikeret ZIP-fil [CVE-2018-7726], hukommelseslækage [CVE-2018-16548]; afviser ZIP-fil, hvis størrelsen på den centrale mappe og/eller offset for begyndelsen af den centrale mappe, peger ud over slutningen af ZIP-filen [CVE-2018-6484, CVE-2018-6541, CVE-2018-6869]">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2018 4259 ruby2.3>
<dsa 2018 4332 ruby2.3>
<dsa 2018 4341 mariadb-10.1>
<dsa 2019 4373 coturn>
<dsa 2019 4374 qtbase-opensource-src>
<dsa 2019 4377 rssh>
<dsa 2019 4385 dovecot>
<dsa 2019 4387 openssh>
<dsa 2019 4388 mosquitto>
<dsa 2019 4389 libu2f-host>
<dsa 2019 4390 flatpak>
<dsa 2019 4391 firefox-esr>
<dsa 2019 4392 thunderbird>
<dsa 2019 4393 systemd>
<dsa 2019 4394 rdesktop>
<dsa 2019 4396 ansible>
<dsa 2019 4397 ldb>
<dsa 2019 4398 php7.0>
<dsa 2019 4399 ikiwiki>
<dsa 2019 4400 openssl1.0>
<dsa 2019 4401 wordpress>
<dsa 2019 4402 mumble>
<dsa 2019 4403 php7.0>
<dsa 2019 4405 openjpeg2>
<dsa 2019 4406 waagent>
<dsa 2019 4407 xmltooling>
<dsa 2019 4408 liblivemedia>
<dsa 2019 4409 neutron>
<dsa 2019 4410 openjdk-8>
<dsa 2019 4411 firefox-esr>
<dsa 2019 4412 drupal7>
<dsa 2019 4413 ntfs-3g>
<dsa 2019 4414 libapache2-mod-auth-mellon>
<dsa 2019 4415 passenger>
<dsa 2019 4416 wireshark>
<dsa 2019 4417 firefox-esr>
<dsa 2019 4418 dovecot>
<dsa 2019 4419 twig>
<dsa 2019 4420 thunderbird>
<dsa 2019 4422 apache2>
<dsa 2019 4423 putty>
<dsa 2019 4424 pdns>
<dsa 2019 4425 wget>
<dsa 2019 4426 tryton-server>
<dsa 2019 4427 samba>
<dsa 2019 4428 systemd>
<dsa 2019 4429 spip>
<dsa 2019 4430 wpa>
<dsa 2019 4431 libssh2>
<dsa 2019 4432 ghostscript>
<dsa 2019 4433 ruby2.3>
<dsa 2019 4434 drupal7>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>				<th>Årsag</th></tr>
<correction gcontactsync 			"Inkompatibel med nyere versioner af firefox-esr">
<correction google-tasks-sync 			"Inkompatibel med nyere versioner af firefox-esr">
<correction mozilla-gnome-kerying 		"Inkompatibel med nyere versioner af firefox-esr">
<correction tbdialout 				"Inkompatibel med nyere versioner af thunderbird">
<correction timeline 				"Inkompatibel med nyere versioner af thunderbird">
</table>


<h2>Debian Installer</h2>

Installeringsprogrammet er opdateret for at medtage rettelser indført i stable, 
i denne punktopdatering.


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Foreslåede opdateringer til den stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Oplysninger om den stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
