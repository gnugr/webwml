#use wml::debian::projectnews::header PUBDATE="2018-12-11" SUMMARY="Bienvenue sur les Nouvelles du projet Debian, publication de la mise à jour de Debian 9.6, sauvetage de paquet, les constructions reproductibles rejoignent Software Conservancy, Rust disponible sur 14 architectures Debian, décision du Comité technique, calendrier du gel à venir, brèves de l'équipe anti-harcèlement, chasses aux bogues pour Buster, comptes-rendus, demandes d'aide, au-delà du code, liens rapides vers des médias sociaux de Debian"
#use wml::debian::acronyms
#use wml::debian::translation-check translation="09966b2f1ad834f67126c0f4a6f131d6f995ec06" maintainer="Jean-Pierre Giraud"

# $Id$
# $Rev$
# Status: [content-frozen]


## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<intro issue="quatrième" />
<toc-display/>

<toc-add-entry name="newdpn">Bienvenue sur les Nouvelles du projet Debian !</toc-add-entry>

<p>Nous espérons que vous appréciez cette édition des Nouvelles du projet
Debian.</p>

<p>Pour d'autres nouvelles, veuillez consulter le blog officiel de Debian
<a href="https://bits.debian.org">Bits from Debian</a> et suivre
<a href="https://micronews.debian.org">https://micronews.debian.org</a> qui
alimente aussi (par RSS) le profil @debian de plusieurs réseaux sociaux.</p>
<p>À la fin de ces nouvelles du projet, nous avons ajouté une section
<b>Liens rapides</b> qui renvoient à une sélection des messages envoyés sur
nos autres flux d'informations.</p>

<p>L'équipe de sécurité de Debian diffuse au jour le jour les annonces de
sécurité (<a href="$(HOME)/security/2018/">annonces de sécurité 2018</a>).
Nous vous conseillons de les lire avec attention et de vous inscrire à la
<a href="https://lists.debian.org/debian-security-announce/">liste de diffusion correspondante</a>.</p>


<toc-add-entry name="internal">Nouvelles internes et événements</toc-add-entry>

<p><b>Mise à jour de Debian 9.6</b></p>
<p>Le projet Debian a <a href="https://www.debian.org/News/2018/20181110">annoncé</a>
la sixième mise à jour de la distribution stable Debian 9 (nom de code Stretch)
le 10 novembre 2018 vers la version intermédiaire 9.6.</p>

<p>Cette version intermédiaire ajoute des corrections à des problèmes de sécurité,
tout en réglant quelques problèmes importants. Les annonces de sécurité ont déjà
été publiées séparément.

La mise à niveau d'une installation vers cette révision se fait en faisant
pointer l'outil de gestion des paquets vers l'un des nombreux miroirs HTTP de Debian.
Une liste complète des miroirs est disponible à l'adresse :
<a href="https://www.debian.org/mirror/list">https://www.debian.org/mirror/list</a>.
</p>


<p><b>Sauvetage de paquet</b></p>

<p>Tobias Frost <a href="https://lists.debian.org/debian-devel-announce/2018/09/msg00003.html">a annoncé</a>
un nouvel ajout à la Référence du développeur Debian sur le sauvetage de paquet.
Le <a href="https://www.debian.org/doc/manuals/developers-reference/ch05.en.html#package-salvaging">sauvetage de paquet</a>
permet que des paquets qui ne sont pas officiellement déclarés orphelins
ou abandonnés soient maintenus par d'autres développeurs ou de nouveau contributeurs
après que certains critères d'éligibilité aient été remplis.</p>

<p>Ce processus diffère de la gestion des paquets dont le responsable est manquant
à l'appel (MIA) en ce qu'il permet à des paquets négligés ou oubliés de rentrer dans
le rang. Il existe un ensemble de <a href="https://wiki.debian.org/PackageSalvaging">règles</a>
disponibles qui présentent les différentes étapes du processus avec des informations
supplémentaires et des FAQ sur le wiki Debian.</p>


<p><b>Les constructions reproductibles rejoignent Software Conservancy !</b></p>

<p>Les <a href="https://reproducible-builds.org/">constructions reproductibles</a> rejoignent
<a href="https://sfconservancy.org/about/">Software Freedom Conservancy</a>, une organisation
à but non lucratif de type juridique US « 501(c)(3) », qui contribue à promouvoir, améliorer,
développer et défendre les projets de logiciel libre et à code ouvert (FLOSS). Au travers du
SFC les projets membres peuvent recevoir des dons affectés au bénéfice de projets FLOSS
particuliers.</p>

<p>Le projet des constructions reproductibles qui <a href="https://wiki.debian.org/ReproducibleBuilds/History">a débuté
comme un projet</a> au sein de la communauté Debian est aussi crucial pour le travail
de conformité propre à Conservancy : une construction qui ne peut pas être vérifiée
peut renfermer du code qui déclenche des responsabilités de conformité de licence
différentes de celles auxquelles le bénéficiaire s'attend.</p>

<p>Comme les constructions reproductibles rejoignent Conservancy, elles reçoivent aussi
un don de 300 000 $ US de la <a href="https://handshake.org/">Handshake Foundation</a> qui
va stimuler les efforts du projet pour assurer la santé et la facilité d’emploi futures du
logiciel libre.</p>

<p><b>Rust disponible sur 14 architectures Debian</b></p>

<p>John Paul Adrian Glaubitz a annoncé la <a
href="https://lists.debian.org/debian-devel-announce/2018/11/msg00000.html">disponibilité
de Rust sur 14 architectures Debian</a> et a remercié les nombreux contributeurs
qui ont aidé à y parvenir. Les <a
href="https://buildd.debian.org/status/package.php?p=rustc&amp;suite=unstable">dernières</a>
architectures prises en charge sont mips, mips64el, mipsel et powerpcspe.</p>
<p>Ce travail est le résultat de l'effort combiné de nombreuses personnes
talentueuses et leur travail sur l'amont de LLVM qui a corrigé de très nombreux
bogues dans les dorsaux MIPS et PowerPC ainsi que l'ajout de la prise en charge
de la sous-cible PowerPCSPE.</p>

<p><b>Documentation sur les détenteurs de copyright dans debian/copyright</b></p>

<p>L'équipe FTP a publié <a
href="https://lists.debian.org/debian-devel-announce/2018/10/msg00004.html">des éclaircissements</a>
concernant l'attribution du copyright dans debian/copyright. Voici les principaux points :</p>

<ul>
<li>À moins qu'une licence mentionne explicitement que les attributions de copyright
ne s'appliquent qu'aux distributions de sources, elles s'appliquent de la même manière
au source et au binaire. Le copyright doit être documenté dans debian/copyright pour
des raisons de conformité de licence.</li>

<li>Gardez à l'esprit le chapitre <a
href="https://www.debian.org/doc/debian-policy/ch-archive.html#copyright-considerations">2.3 Considérations sur le copyright</a> :
Tous les paquets doivent être accompagnés par une copie intégrale des informations de
copyright et de la licence de distribution dans le fichier /usr/share/doc/package/copyright.</li>

<li>À de rares occasions, les membres de « FTP masters » ont statué que l'attribution complète
du copyright était à la fois irréalisable et que, étant donnée la nature du paquet, une
notice de copyright adéquate n'avait pas besoin de lister la totalité des détenteurs ;
dans de tels cas, il ne faut pas penser que cette tolérance s'applique à d'autres paquets.</li>
</ul>

<p>L'équipe FTP maintient que la documentation des détenteurs du copyright dans debian/copyright
est une bonne idée.</p>

<p><b>Décision du Comité technique sur les séries de correctifs particulières à un fournisseur</b></p>

<p>Le comité technique <a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00004.html">a adopté une résolution</a> sur l'opportunité d'utiliser les séries de correctifs
particulières à un fournisseur dans l'archive Debian, en résumé :</p>

<p>Le comité reconnaît la nécessité pour les paquets de se comporter différemment
dans des distributions différentes lors de leur construction, mais cela devrait
être obtenu en utilisant des paquets source différents, ou comme partie du processus
de construction, en utilisant des pratiques actuelles et futures telles que des
correctifs avec un comportement conditionnel, ou l'application de correctifs à des
fichiers durant la construction plutôt qu'au moment du dépaquetage des sources.</p>

<p>Dans la mesure où cette fonction est utilisée dans plusieurs paquets aujourd'hui,
une période raisonnable de transition s'avère nécessaire. Néanmoins, ces paquets
seront considérés comme bogués dès l'adoption de cette résolution, mais la
sévérité de ce bogue ne sera pas jugée suffisante pour justifier un retrait
immédiat de Debian.</p>

<p>Après la publication de Buster, la présence de séries de correctifs particulières
à un fournisseur sera une violation d'une directive « MUST » de la Charte Debian.</p>

<p>En conséquence, le comité adopte la résolution : toute utilisation de la
fonction de séries de correctifs pour dpkg, particulières à un fournisseur, est
considérée comme un bogue pour les paquets dans l’archive de Debian (y compris
contrib et non-free).</p>

<p>Après la publication de Buster, l'utilisation de la fonction de séries de
correctifs particulières à un fournisseur est interdite dans l'archive Debian.</p>

<p>Pour des informations complémentaires et la discussion originale veuillez
consulter le <a href="https://bugs.debian.org/904302">bogue n° 904302</a>.</p>

<p><b>Équipe en charge de la publication : calendrier du gel à venir, comment aider</b></p>

<p>L'équipe en charge de la publication <a href="https://lists.debian.org/debian-devel-announce/2018/09/msg00004.html">prépare</a>
la phase initiale du gel de Buster.</p>

<p>Il est rappelé aux développeurs qu'ils doivent suivre leur plan et évaluer de
façon réaliste leur calendrier pour réaliser leurs modifications et les inclure dans
Buster.</p>

<p>Les modifications peuvent être réalisées dans experimental pour éviter toute
perturbation. Il faut garder à l'esprit que les autres bénévoles peuvent ne pas avoir
les mêmes disponibilités pour travailler sur vos objectifs. Il est suggéré que tous les
bogues non corrigés le soient grâce aux <a
href="https://wiki.debian.org/NonMaintainerUpload">NMU</a> dès maintenant.</p>

<p>Le calendrier officiel du gel de Buster est le suivant :</p>
<ul>
 <li>12-01-2019 – gel de transition</li>
 <li>12-02-2019 – gel doux</li>
 <li>12-03-2019 – gel complet</li></ul>

<p>Veuillez consulter la <a href="https://release.debian.org/buster/freeze_policy.html">politique de gel</a> de Buster et le calendrier pour des informations détaillées sur les différents types
de gel et ce qu'ils signifient pour vous.</p>

<p>Si vous voulez nous aider à publier Buster dans les délais et pouvez aider à
corriger des bogues critiques dans testing avant le gel de transition, vous pouvez
le faire maintenant en consultant la <a href="https://udd.debian.org/bugs/">liste des
bogues critiques</a> ou en rejoignant le canal IRC &#35;debian-bugs sur irc.oftc.net.</p>

<p><b>Brèves de l'équipe de Debian anti-harcèlement</b></p>

<p><a href="https://wiki.debian.org/AntiHarassment">L'équipe Debian anti-harcèlement</a> est
le point de contact pour tous les membres de la communauté qui aimeraient contribuer
à créer un environnement plus accueillant et respectueux dans Debian et c'est aussi
le point de contact pour des signalements ou des inquiétudes sur des comportements
inappropriés ou des abus. L'équipe enverra à la communauté des comptes-rendus courts
mais réguliers.</p>

<p>Si vous constatez des échanges que vous considérez devoir retenir l'attention,
faites le nous savoir. N'attendez pas que le problème enfle trop, nous pouvons intervenir
amicalement pour désamorcer le conflit ou comme médiateurs. Les membres peuvent aussi
rapporter des informations sur un problème pour lequel aucune intervention ne sera
effectuée, mais à retenir au cas où celui-ci enflerait à un moment quelconque à l'avenir.</p>

<p>L'équipe peut être contactée à l'adresse <a
href="mailto:antiharassment@debian.org">antiharassment@debian.org</a>.</p>

<p>Quelques points forts de notre activité récente :</p>

<p>une demande d'intervention sur un conflit à propos d'un paquet jugé offensant,
pour laquelle nous avons publié une recommandation : <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=907199">bogue n° 907199</a></p>

<p>Nous avons été saisis pour retirer des messages d'une liste de diffusion et avons
répondu.</p>

<p>Plusieurs participants à la DebConf 18 ont joué le rôle d'équipe locale contre le
harcèlement, et ont géré des conflits entre participants et une violation potentielle
du Code de conduite, arbitrant des problèmes mineurs et offrant leur service durant
la conférence.</p>

<p>Un rappel général sur le Code de conduite a été envoyé par micronews à plusieurs
reprises pendant la DebConf 18.</p>

<p>Nous avons été impliqués dans les discussions sur la politique de photo pour la
DebConf, et nous projetons de faire une proposition prochainement.</p>

<p><b>Nouvelle stagiaire Outreachy</b></p>

<p>Debian souhaite la bienvenue à <a
href="https://bits.debian.org/2018/11/welcome-outreachy-intern-2018-2019.html">Anastasia Tsikoza
notre nouvelle stagiaire Outreachy</a>. Le programme Outreachy offre des stages
à des personnes appartenant à des groupes traditionnellement sous-représentés
dans le domaine de la technologie. Anastasia est parrainée par Paul Wise et Raju
Devidas, et travaillera sur l'amélioration de l'intégration des dérivés de Debian
à l'infrastructure de Debian et à sa communauté.</p>


<p><b>Nouvelles diverses pour les développeurs</b></p>

<p>Paul Wise <a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00002.html">a envoyé</a>
le dernier numéro des Nouvelles diverses pour les développeurs (n° 46). Parmi les
points forts :</p>

<p>Le mode de compatibilité 12 de debhelper est ouvert aux tests bêta et devrait
devenir stable dans Debian Buster.</p>

<p>Un nouveau portage pour RISC-V[2] saveur « riscv64 » (64 bits petit boutien) est
maintenant disponible dans les portages Debian.</p>

<p>Avec la publication récente de debcargo 2.0.0 pour crates.io, les paquets Debian
peuvent être créés à partir de votre « crate » de Rust préféré et envoyés dans l'archive
Debian.</p>

<p>La version devscripts 2.18.5 est parue et apporte de nouvelles fonctionnalités
d'uscan telles que la vérification des étiquettes signées dans git et des valeurs
automatiques pour dversionmangle.</p>

<p>Chris Lamb lance un appel pour que plus de volontaires rejoignent l'équipe FTP.</p>


<toc-add-entry name="events">Événements à venir et comptes rendus</toc-add-entry>

<p><b>Événements à venir</b></p>

<p><b>MiniDebConf 2019 à Marseille</b></p>
<p>
Une miniDebConf se tiendra à Marseille (France) les 25 et 26 mai, avec deux jours
de communication, des communications éclairs, une séance de signature de clés, un
repas et une soirée bière… Consultez <a
href="https://france.debian.net/pipermail/minidebconf/2018-November/000159.html">l'annonce</a>
et visitez la <a href="https://wiki.debian.org/DebianEvents/fr/2019/Marseille">page wiki de l'évènement</a>
où vous pourrez trouver tous les détails nécessaires, vous inscrire à la manifestation
et aider à son organisation.</p>

<p><b>Des chasses aux bogues pour Buster !</b></p>

<p>De nombreuses chasses aux bogues s'inscrivent dans nos agendas et mettent
l'accent sur des efforts pour traiter et corriger les bogues critiques qui peuvent
retarder la publication de Debian 10 (Buster). Les chasses aux bogues sont ouvertes
à tous ceux qui souhaitent et sont capables de s'investir. Venez nous aider à faire
de cette publication un succès !</p>

<p><b>Pays-Bas, Venlo, les 12 et 13 janvier 2019</b></p>

<p>Hébergé par Transceptor Technology et insign.it.</p>

<p>Soyez bienvenu si vous souhaitez contribuer à Debian, quel que soit votre
niveau de compétence. Il n'est pas nécessaire d'être contributeur Debian actuel.
Juste tenter de reproduire un bogue et documenter cette expérience est déjà utile.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00005.html">Annonce de la chasse aux bogues de Venlo</a></p>
<p>Page <a href="https://wiki.debian.org/BSP/2019/01/nl/Venlo">wiki de la chasse aux bogues de Venlo</a></p>

<p><b>Canada, Montréal, 19 et 20 janvier 2019</b></p>

<p>Hébergé par Eastern Bloc, Montréal, Canada.</p>

<p>À la différence de celle que nous avons organisée pour la publication de Stretch,
cette chasse aux bogues se tiendra pendant un week-end complet, ainsi, avec un peu
de chance, des personnes d'autres provinces au Canada ou des États-Unis pourront
nous rejoindre. </p>
<p>Vous pouvez vous inscrire sur la page wiki où vous trouverez des informations sur
les transports, l'hébergement, la nourriture et d'autres choses utiles. Vos frais
de participation à cette chasse aux bogues pourront être sponsoriés par le projet
Debian.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/12/msg00000.html">Annonce de la chasse aux bogues de Montreal</a></p>
<p>Page <a href="https://wiki.debian.org/BSP/2019/01/ca/Montreal">wiki de la chasse aux bogues de Montréal</a></p>


#<p>
#venue yet unknon
#There will be a <a href="https://wiki.debian.org/BSP/2019/01/us/Austin">Bug
#Squashing Party</a> on 25-27 January 2019, held in Austin, Texas, USA.
#</p>

<p><b>Allemagne, Bonn, du 22 au 24 février 2019</b></p>

<p>Tarent solutions GmbH, Rochussstr. 2, 53123 Bonn, Allemagne</p>

<p>La chasse aux bogues est programmée juste entre le gel « doux » et le gel complet,
et est donc une occasion parfaite pour une chasse vraiment efficace et centrée sur les
bogues critiques.</p>

<p>Le lieu d'accueil offre de la place pour 20 personnes, des pièces séparées
pour ceux qui veulent coder en équipe plus réduite, ainsi qu'une pièce pour se rencontrer.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00001.html">Annonce pour la chasse aux bogues de Bonn</a></p>
<p>Page <a href="https://wiki.debian.org/BSP/2019/02/de/Bonn">wiki de la chasse aux bogues de Bonn</a></p>

<p><b>Autriche, Salzbourg, du 5 au 7 avril 2019</b></p>

<p>Bureaux de Conova Communications GmbH [CONOVA], situés près de l'aéroport W.A. Mozart
de Salzbourg.</p>

<p>Nous sommes heureux de vous convier à la 6e chasse aux bogues Debian à
Salzbourg, Autriche.</p>

<p>Une inscription rapide sur la page wiki [BSPSBG] est demandée pour faciliter
l'organisation de la manifestation. Sur la même page vous trouverez des informations
sur le transport, l'hébergement (sponsorisé) et d'autres choses utiles.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00006.html">Annonce pour la chasse aux bogues de Salzbourg</a></p>
<p>Page <a href="https://wiki.debian.org/BSP/2019/04/Salzburg">wiki de la chasse aux bogues de Salzbourg</a></p>

<toc-add-entry name="reports">Comptes rendus</toc-add-entry>

<p><b>Comptes rendus mensuels de Freexian sur Debian Long Term Support (LTS)</b></p>

<p>Freexian publie <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">des comptes rendus mensuels</a>
sur le travail réalisé par les contributeurs salariés pour le suivi à long
terme de sécurité de Debian.
</p>

<p><b>État d'avancement des compilations reproductibles</b></p>

<p>Suivez le <a href="https://reproducible-builds.org/blog/">blog des compilations reproductibles</a>
pour obtenir un compte rendu de leur travail sur le cycle de <q>Buster</q>.
</p>


<toc-add-entry name="help">Demandes d'aide !</toc-add-entry>

<p><b>Paquets qui ont besoin de travail</b></p>

<wnpp link="https://lists.debian.org/debian-devel/2018/11/msg00733.html"
	orphaned="1311"
	rfa="157" />

<p><b>Bogues pour débutants</b></p>

<p>
Debian utilise l'étiquette <q>newcomer</q> (débutant) pour signaler les
bogues qui sont adaptés aux nouveaux contributeurs comme point d'entrée pour
travailler sur des paquets particuliers.

Il y a actuellement <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">212</a> bogues
marqués <q>newcomer</q> disponibles.
</p>

<toc-add-entry name="morethancode">Par-delà le code</toc-add-entry>

<p>Carl Chenet attire l'attention sur : <a
href="https://carlchenet.com/you-think-the-visual-studio-code-binary-you-use-is-a-free-software-think-again/">Vous pensez
que le binaire de Visual Studio Code que vous utilisez est un logiciel libre ? Détrompez-vous.</a>
Il montre certaines des pratiques de licence utilisées comparées à la licence MIT,
une licence libre permissive.</p>

<p>Elana Hashman partage des informations sur les <a href="https://hashman.ca/pygotham-2018/">sources
de sa communication à PyGotham 2018</a> intitulée « The Black Magic of Python Wheels »,
basée sur ses deux ans de travail sur auditwheel et la plateforme manylinux.</p>

<p>Benjamin Mako Hill parle de <a
href="https://mako.cc/copyrighteous/what-we-lose-when-we-move-from-social-to-market-exchange">Ce que nous
perdons quand nous passons de l'échange social au marché</a>, au sujet de l'échange
d'argent contre un autre bien par rapport à l'échange d'hospitalité.</p>

<p>Molly de Blanc explique comment elle est parvenue mettre en place le programme
de donation à parité <a href="http://deblanc.net/blog/2018/11/24/conservancy-match/">Conservancy</a>
au profit de « Software Freedom Conservancy ».</p>


<toc-add-entry name="code">Code, programmeurs et contributeurs</toc-add-entry>
<p><b>Nouveaux responsables de paquets depuis le 19 août 2018</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD

<p>Bienvenue à Pedro Loami Barbosa dos Santos, Alexandros Afentoulis,
David Kunz, Helen Koike, Andreas Schwarz, Miriam Retka, GreaterFire, Birger
Schacht, Simon Spöhel, Guillaume Pernot, Joachim Nilsson, Mujeeb Rahman K, Timo
Röhling, Hashem Nasarat, Christophe Courtois, Matheus Faria, Oliver Dechant,
Johan Fleury, Gabriel Filion, Baptiste Beauplat, Bastian Germann, Markus
Wurzenberger, Jeremy Finzel, Mangesh Divate, Jonas Schäfer, Julian Rüth,
Scarlett Moore, Tiago Stürmer Daitx, Tommi Höynälänmaa, Romuald Brunet,
Gerardo Ballabio, Stewart Ferguson, Julian Schauder, Chen-Ying Kuo, Denis
Danilov, David Lamparter et Kienan Stewart.</p>

<p><b>Nouveaux responsables Debian</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD

<p>Bienvenue à Sagar Ippalpalli, Kurt Kremitzki, Michal Arbet, Peter Wienemann,
Alexis Bienvenüe et Gard Spreemann.</p>

<p><b>Nouveaux développeurs Debian</b></p>
<p>Bienvenue à Joseph Herlant, Aurélien Couderc, Dylan Aïssi, Kunal Mehta,
Ming-ting Yao Wei, Nicolas Braud-Santoni, Pierre-Elliott Bécue, Stephen Gelman,
Daniel Echeverry et Dmitry Bogatov.</p>


<p><b>Contributeurs</b></p>

<p>
1603 personnes et 19 équipes sont actuellement recensées dans la page des
<a href="https://contributors.debian.org/">contributeurs</a> Debian pour 2018.
</p>


<p><b>Statistiques</b></p>
##Pull this information from sources.d.o
##https://sources.debian.org/stats/sid/
##https://sources.debian.org/stats/buster/
<p><b><em>Buster</em></b></p>
<ul style="list-style-type:none">
<li>Fichiers source : 11 899 818</li>
<li>Paquets source : 28 675</li>
<li>Espace disque : 252 942 632 kO</li>
<li>Ctags : 17 804 273</li>
<li>Lignes de code source : 1 045 062 355</li>
</ul>

<p><b><em>Sid</em></b></p>
<ul>
<li>Fichiers source : 20 123 470</li>
<li>Paquets source : 33 506</li>
<li>Espace disque : 367,564,896 kO</li>
<li>Ctags : 42,263,242</li>
<li>Lignes de code source : 1 764 065 923</li>
</ul>


<p><b>Discussions</b></p>

<p>Aurélien Couderc, utilisateur de Debian, a posé une question sur le <a
href="https://lists.debian.org/debian-devel/2018/09/msg00220.html">changement d'époque
et réutilisation d'un nom de paquet</a>,
ce qui a mené à une discussion sur la demande de modifications amont sur les
règles internes de Debian et leur effet pour les utilisateurs. Plusieurs
alternatives ont été mentionnées durant la discussion ainsi que les pièges posés
par les sauts de numéro de version.</p>


<p>Pétùr, utilisateur de Debian a demandé de l'aide au sujet d'un <a
href="https://lists.debian.org/debian-user/2018/09/msg00311.html">fichier avec des
droits bizarres, impossible à supprimer</a>.
La discussion a dérivé rapidement sur les problèmes de droits, les inodes, fsck
et les câbles SATA de mauvaise qualité.</p>


<p>Subhadip Ghosh, utilisateur de Debian, a posé la question : <a
href="https://lists.debian.org/debian-user/2018/09/msg00700.html">Pourquoi Debian
permet-elle par défaut tout trafic entrant ?</a></p>


<p><b>Trucs et astuces</b></p>

<p>Jonathan McDowell poursuit sa série d'articles sur la domotique avec un envoi sur le
<a href="https://www.earth.li/~noodles/blog/2018/10/heating-automation.html">contrôle
du chauffage avec Home Assistant</a>
et un autre sur <a href="https://www.earth.li/~noodles/blog/2018/09/netlink-arp-presence.html">l'utilisation
d'ARP avec netlink pour la détection de présence.</a>

<p>Antoine Beaupré partage des conseils sur <a href="https://anarc.at/blog/2018-10-04-archiving-web-sites/">l'archivage
de sites web</a> avec des outils libres d'accès et quelques connaissances.</p>

<p>Sergio Alberti <a href="https://sergioalberti.gitlab.io//gsoc/debian/2018/09/24/reveng.html">partage</a> un guide sur
<a href="https://reverse-engineering-ble-devices.readthedocs.io/en/latest/">rétro-ingénierie
de périphériques Bluetooth à basse consommation</a>.</p>

<p>Petter Reinholdtsen <a href="http://people.skolelinux.org/pere/blog/VLC_in_Debian_now_can_do_bittorrent_streaming.html/">nous informe que VLC dans Debian
peut maintenant gérer les flux par BitTorrent</a>.</p>

<p>Laura Arjona Reina a retrouvé un petit cadre photo numérique et l'a remis en
marche : <a href="https://larjona.wordpress.com/2018/09/22/handling-an-old-digital-photo-frame-ax203-with-debian-and-gphoto2/">gestion
d'un vieux cadre photo numérique (AX203) avec Debian (et gphoto2)</a>.</p>

<p><b>Il était une fois dans Debian :</b></p>

<ul>
<li>le 8 décembre 2009, <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=560000">le bogue de Debian n° 560 000 a été rapporté par Mika Tiainen</a> ;</li>
<li>le 10 décembre 2016, <a href="https://miniconf.debian.or.jp/index.en.html">la MiniDebconf 2016 s'est tenue à Tokyo, Japon</a> ;</li>
<li>le 12 décembre 1996, <a href="https://lists.debian.org/debian-devel-announce/2014/08/msg00012.html">publication de Debian 1.2 (Rex)</a> ;</li>
<li>le 13 décembre 2014, <a href="https://wiki.debian.org/BSP/2014/12/nl/Tilburg">chasse aux bogues à Tilbourg, Pays-Bas</a> ;</li>
<li>le 13 décembre 2016, <a href="https://reproducible-builds.org/events/berlin2016/">Debian coorganise et parraine le Reproducible Builds Summit à Berlin, Allemagne</a>.</li>
</ul>


<toc-add-entry name="outside">Nouvelles de l'extérieur</toc-add-entry>

<p>
Le Creative Commons Global Summit se tiendra à Lisbonne, Portugal, du 9 au
11 mai 2019. L'appel à communications est ouvert jusqu'au 10 décembre 2018.
Consultez <a href="https://summit.creativecommons.org/">https://summit.creativecommons.org/</a>
pour plus de détails et vous inscrire.
</p>


<toc-add-entry name="quicklinks">Liens rapides vers des médias sociaux de Debian</toc-add-entry>
<p>
Voici un extrait du fil
<a href="https://micronews.debian.org">micronews.debian.org</a>
d'où nous avons retiré les sujets déjà commentés dans ce numéro des
Nouvelles du projet Debian. Vous pouvez sauter cette rubrique si vous
suivez déjà <b>micronews.debian.org</b> ou le profil <b>@debian</b> sur un
réseau social (Pump.io, GNU Social, Mastodon ou Twitter).
Ces sujets sont livrés non formatés par ordre chronologique du plus
récent au plus ancien.
</p>

<p>
<b>Novembre</b></p>
<ul>

<li>Brèves du Chef du projet @Debian (novembre 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00007.html">https://lists.debian.org/debian-devel-announce/2018/11/msg00007.html</a>
</li>

<li>Un « pipeline » d'intégration continue (CI) de Debian pour les responsables Debian !
<a href="https://salsa.debian.org/salsa-ci-team/pipeline/blob/master/README.md">https://salsa.debian.org/salsa-ci-team/pipeline/blob/master/README.md</a>
</li>

<li>Joyeux anniversaire @Fedora !
<a href="https://fedoramagazine.org/celebrate-fifteen-years-fedora/">https://fedoramagazine.org/celebrate-fifteen-years-fedora/</a>
</li>

<li>La transition vers Perl 5.28 est entamée, on doit s'attendre à une grande instabilité de Sid ces prochains jours !
<a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00006.html">https://lists.debian.org/debian-devel-announce/2018/10/msg00006.html</a>
</li>
</ul>


<p>
<b>Octobre</b></p>
<ul>
<li>Brèves du Chef du projet Debian (octobre 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00005.html">https://lists.debian.org/debian-devel-announce/2018/10/msg00005.html</a>
</li>

<li>« Des rubans pour Salsa » par Chris Lamb
<a href="https://chris-lamb.co.uk/posts/salsa-ribbons">https://chris-lamb.co.uk/posts/salsa-ribbons</a>
</li>

<li>[debian-installer] : Appel à la mise à jour des traductions pour Buster
<a href="https://lists.debian.org/debian-i18n/2018/10/msg00002.html">https://lists.debian.org/debian-i18n/2018/10/msg00002.html</a>
</li>

<li>Brèves de la MicroDebConf Brasília 2018
<a href="http://blog.kanashiro.xyz/debconf/2018/09/28/microdebconf-bsb.html">http://blog.kanashiro.xyz/debconf/2018/09/28/microdebconf-bsb.html</a>
</li>
</ul>


<p>
<b>Septembre</b></p>
<ul>
<li>Brèves du Chef du projet Debian (septembre 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/09/msg00005.html">https://lists.debian.org/debian-devel-announce/2018/09/msg00005.html</a>
</li>

<li>Appel à parrainage et idées de projet pour la prochaine édition du programme Outreachy
<a href="https://lists.debian.org/debian-outreach/2018/09/msg00030.html">https://lists.debian.org/debian-outreach/2018/09/msg00030.html</a>
</li>

<li>Les dépôts de sécurité de Debian sont restés en ligne au Japon malgré un tremblement de terre de magnitude 6,7
<a href="https://henrich-on-debian.blogspot.com/2018/09/earthquake-struck-hokkaido-and-caused.html">https://henrich-on-debian.blogspot.com/2018/09/earthquake-struck-hokkaido-and-caused.html</a>
</li>
</ul>


<p>
<b>Août</b></p>
<ul>
<li>Le FISL19 à Porto Alegre se tiendra avant le DebCamp de l'an prochain
<a href="https://debconf19.debconf.org/news/2018-08-23-fisl19/">https://debconf19.debconf.org/news/2018-08-23-fisl19/</a>
</li>

<li>Brèves du Chef du projet Debian (septembre 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/08/msg00008.html">https://lists.debian.org/debian-devel-announce/2018/08/msg00008.html</a>
</li>
</ul>

<toc-add-entry name="continuedpn">Continuer à lire les Nouvelles du projet Debian</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">S'inscrire ou se désinscrire </a> de la liste de diffusion Debian News</p>

#use wml::debian::projectnews::footer editor="l'équipe en charge de la publicité avec des contributions de Jean-Pierre Giraud, Justin B Rye, Laura Arjona Reina." translator="Jean-Pierre Giraud, l\'équipe francophone de traduction"
