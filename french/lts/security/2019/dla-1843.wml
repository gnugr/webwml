#use wml::debian::translation-check translation="948a4f735ddf8cc716a423170af7b6ebeb8ea468" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans pdns, un serveur DNS faisant
autorité, qui peuvent avoir pour conséquence un déni de service au moyen
d'enregistrements de zone mal formés et de paquets NOTIFY excessifs dans une
configuration maître/esclave.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10162">CVE-2019-10162</a>

<p>Un problème a été découvert dans le serveur d’autorités PowerDNS permettant
à un utilisateur autorisé de provoquer la fin du serveur en insérant un
enregistrement contrefait dans une zone de type MASTER sous son contrôle. Le
problème est dû au fait que le serveur d’autorités abandonnera lorsqu’il
rencontre une erreur d’analyse lors de la recherche d’enregistrements NS/A/AAAA
qu’il est sur le point d’utiliser pour une notification sortante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10163">CVE-2019-10163</a>

<p>Un problème a été découvert dans le serveur d’autorités  PowerDNS permettant
à un serveur maître, autorisé et distant, de provoquer une charge lourde de CPU ou
même d’empêcher de futures mises à jour de n’importe quelle zone esclave en
envoyant un grand nombre de messages NOTIFY. Remarquez que seuls les serveurs
configurés comme esclave sont touchés par ce problème.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.4.1-4+deb8u10.</p>
<p>Nous vous recommandons de mettre à jour vos paquets pdns.</p>

<p>Pour un état de sécurité détaillé de pdns, veuillez vous reporter
à sa page de suivi de sécurité :
<a href="https://security-tracker.debian.org/tracker/pdns">https://security-tracker.debian.org/tracker/pdns</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1843.data"
# $Id: $
