#use wml::debian::translation-check translation="fcad8557c5a7cb7aa7155eaae71ee225a0a2cfe2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Quelques problèmes ont été trouvés dans Atril, le visualisateur de document
de MATE.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000159">CVE-2017-1000159</a>

<p>Lors de l’impression de DVI vers PDF, l’outil dvipdfm était appelé sans
correctement vérifier le nom de fichier. Cela pourrait conduire à une attaque
par injection de commande à l’aide du nom de fichier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>

<p>Tiff_document_render() et tiff_document_get_thumbnail() ne vérifiaient pas
l’état de TIFFReadRGBAImageOriented(), conduisant à un accès mémoire non
initialisé si la fonction échoue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010006">CVE-2019-1010006</a>

<p>Quelques vérifications de dépassement de tampon n’étaient pas correctement
réalisées, conduisant à un plantage d'application ou éventuellement l’exécution
de code arbitraire lors de l’ouverture de fichiers contrefaits de manière
malveillante.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.8.1+dfsg1-4+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets atril.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1882.data"
# $Id: $
